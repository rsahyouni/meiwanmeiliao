package Happy;


public class App {
    public static void main( String[] args )
    {
        Person p = new Person("XiaXia", "Choi", 21);

        String drivingResult = p.Drive();
        System.out.println(drivingResult);

        String talkingResult = p.Talk();
        System.out.println(talkingResult);
    }
}
