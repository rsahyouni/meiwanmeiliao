package Happy;

public class Person {

    private String _firstName;
    private String _lastName;
    private int _age = 0;

    public String GetName() {
        return _firstName + " " + _lastName;
    }

    public int GetAge(){
        return _age;
    }

    public Person(String firstName, String lastName, int age) {
        _firstName = firstName;
        _lastName = lastName;
        _age = age;
    }

    public String Drive(){
        return GetName() + " is driving";
    }

    public String Talk(){
        return GetName() + " is talking";
    }
}
